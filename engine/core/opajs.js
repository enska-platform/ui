/**
 * @namespace App 
 */
Ext.ns('App');
Ext.ns('App.views');
Ext.ns('Widgets');

/**
 * @namespace	opajs-version
 */
opajsVersion = {
	
	/**
	 * Framework name
	 * @type	string
	 */
	name : 'OPAJS',
	
	/**
	 * Framework version
	 * @type	string
	 */
	version : '1.2',
	
	/**
	 * Release type
	 * @type	string
	 */
	releaseType : 'stable',
	
	/**
	 * Release date
	 * @type	string
	 */
	releaseDate : '2016-01'
};

/**
 * @namespace	opajs
 */
opajs = {};

/**
 * @category	Loader
 * @package		engine
 * @license		GPL
 */
(function () {
		
		/**
		 * Add inArray on Array prototype
		 * @param	{Bool}
		 * @return 	{bool}
		 */
		Array.prototype.inArray = (function(p_val) {
		    var l = this.length;
		    for(var i = 0; i < l; i++) { if(this[i] == p_val) { return true; }}
		    return false;
		}) ();
		
		/**
		 * Add htmlEncode on String prototype
		 * @param	{String}
		 * @return 	{bool}
		 */
		String.prototype.htmlEncode = (function() {
		        var entities = {
		            '&': '&amp;', '>': '&gt;', '<': '&lt;', '"': '&quot;'
		        }, keys = [], p, regex;
		        for (p in entities) { keys.push(p); }
		        regex = new RegExp('(' + keys.join('|') + ')', 'g');
		        return function() {
		            return (!this) ? this : this.replace(regex, function(match, capture) { return entities[capture]; });
		        };
		}) ();
		
		/**
		 * Add htmlEncode on String prototype
		 * @param	{String}
		 * @return 	{bool}
		 */
		String.prototype.htmlDecode = (function() {
		        var entities = {
		            '&amp;': '&', '&gt;' : '>', '&lt;' : '<', '&quot;' : '"', '&#41;' : ')', '&#40;' : '('
		        }, keys = [], p, regex;
		        for (p in entities) { keys.push(p); }
		       	regex = new RegExp('(' + keys.join('|') + ')', 'g');
		        return function() {
		            return (!this) ? this : this.replace(regex, function(match, capture) { return entities[capture]; });
		        };
		}) ();
		
		/**
		 * Add urlNameEncode on String prototype
		 * @param	{String}
		 * @return 	{bool}
		 */
		String.prototype.urlNameEncode = (function() {
		        var entities = {
		            ' ': '-', "'": '-', ':': '-', '"': '-', ';': '_', '/': '_', ',':'_',
					'è': 'e', 'é': 'e', '@': '_', '&': '-', 'ç': 'c', 'à': 'a', '!': '-'
		        }, keys = [], p, regex;
		        for (p in entities) { keys.push(p); }
		        regex = new RegExp('(' + keys.join('|') + ')', 'g');
		        return function() {
		            return (!this) ? this : this.replace(regex, function(match, capture) { return entities[capture]; });
		        };
		}) ();
		
		String.prototype.dtp = (function() {
		        var entities = {
		            '-':':'
		        }, keys = [], p, regex;
		        for (p in entities) { keys.push(p); }
		        regex = new RegExp('(' + keys.join('|') + ')', 'g');
		        return function() {
		            return (!this) ? this : this.replace(regex, function(match, capture) { return entities[capture]; });
		        };
		}) ();
		
		opajs = {
			
				/**
				 * opajs loading status
				 * @type	{bool} 
				 */
				isLoaded : false,
				
				/**
				 * Document is ready
				 * @type	{bool} 
				 */
				documentIsReady : false,
				
				/**
				 * Get the framework ename
				 * @return	{string}
				 */
				ename: function () {
					return (this.name + ' ' + this.version + ' (' + this.releaseType + ' / ' + this.releaseDate + ')');
				},
				
				/**
				 * Loading core
				 * @return	{void}
				 */
				enter: function () {
					this._loadServices();
					this._loadLayout();
					this._loadRoutes();
					this.isLoaded = true;
				},
				
				/**
				 * Loading services
				 * @return	{void}
				 */
				_loadServices: function () {
					var obj, services = ['utils', 'services', 'layout', 'routes', 'context', 'proxy', 'bus', 'auth', 'workers'];
					for (var i = 0; i < services.length; i++) {
						if ((typeof(opajs.services[services[i]]) === 'object') && (typeof(opajs.services[services[i]].load) === 'function')) {
							opajs.services[services[i]].load();
							if (services[i] !== 'services') {
								obj = {id: services[i], instance: opajs.services[services[i]]};
								opajs.services.services.services.push(obj);
							}
						}}
				},
				
				/**
				 * Loading layout
				 * @return	{void} 
				 */
				_loadLayout	: function () {
					opajs.services.layout.initViewport();
					opajs.services.layout.render();
				},
				
				/**
				 * Loading routes
				 * @return	{void}
				 */
				_loadRoutes: function () {
					window.onpopstate = function (event) {
						opajs.services.routes._onPopState(event, false);
					};
					opajs.services.routes.loop();
				},
				
				/**
				 * Get a service instance
				 * @param	{String}
				 * @return	{Object}
				 */
				srv: function (serviceName) {
					services = opajs.services.services.get(serviceName);
					if (services) { return (opajs.services.services.get(serviceName).access()); }
					else { return false; }
					
				},
				
				/**
				 * Get a controller instance
				 * @param	{String}
				 * @return	{Object}
				 */
				ctl: function (controllerName) {
					return (opajs.services.routes.getController(controllerName));
				},
				
				/**
				 * Get a view instance
				 * @param	{String}
				 * @return	{Object}
				 */
				vw: function (viewName) {
					return (opajs.services.layout.getView(viewName));
				},
				
				/**
				 * Get a widget instance
				 * @param	{String}
				 * @return	{Object}
				 */
				wdg: function (widgetName) {
					return (opajs.services.layout.getWidget(widgetName));
				},
				
				/**
				 * Get a tpl
				 * @param	{String}
				 * @return	{String}
				 */
				tpl: function (tplName) {
					return (opajs.services.layout.getTpl(tplName));
				},
				
				/**
				 * Get a store
				 * @param	{String}
				 * @return	{String}
				 */
				str: function (storeName) {
					return (opajs.services.storage.getStore(storeName));
				}
			}
		
		/**
		 * Framework reference access
		 */
		if (!window.opajs) {
			window.opajs = opajs;
		}
		
		if (!window.O) {
			window.O = opajs;
		}
	}
) ();

/**
 * Console.log alias 
 * @param {mixed}
 * @return	{void}
 */
function ln(datas) {
	console.log(datas);
}

/**
 * Console.dir alias 
 * @param {mixed}
 * @return	{void}
 */
function dir(datas) {
	console.dir(datas);
}

/**
 * Console.warn alias 
 * @param {mixed}
 * @return	{void}
 */
function warn(datas) {
	console.warn(datas);
}

/**
 * get services instance alias
 * @return	{mixed}
 */
function services() { opajs.services.services; }

/**
 * Translate string
 * @param	{String}
 * @param	{String}
 * @return	{String}
 */
var translate = function (str, local) {
	var r = (local) ? 'I18N_' + local : 'I18N_' + Config.i18n.def;
	try {
		r = eval(r + '.' + str);
		if (typeof r === 'undefined') { r = str; }
	}
	catch(e) { r = str }
	return (r);
}