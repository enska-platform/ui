/**
 * @category	Helper
 * @package		engine.helpers
 * @namaspace	opajs.helpers
 */

Ext.ns('opajs.helpers');

opajs.helpers.view = Ext.extend(opajs.helpers.model, {
		
		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.view}
		 */
		constructor : function (config) {
			if (config) { Ext.apply(this, config); }
			opajs.helpers.view.superclass.constructor.apply(this);
			this.widgets = new opajs.helpers.store();
			this.__load();
			this.init();
		},
		
		/**
		 * Init the view
		 * @return	{void}
		 */
		__load	: function () {
			if (!this.config) { this.config = {}; }
			if (this.config.meta) {this.__loadMeta(); return; }
			this.__initTemplate();
			this.__loadWidgets();
		},
		
		/**
		 * Load the view data
		 * @param	{bool}
		 * @return	{void} 
		 */
		__loadMeta	: function () {
			if (this.config.meta) {
				var self = this;
				this.meta.request(this.config.meta, 
				function (meta, records, error, raw, resObj, config) {
					if (meta) { self.meta.sets(meta.gets()); }
					if (records) { self.records.sets(records.gets()); }					
					self.__initTemplate(meta);
					if (self.config.widgets) { self.__loadWidgets(); }
				}, this.getId());
			}
		},
		
		/**
		 * Init the template system
		 * @param	{Object}
		 * @return	{void} 
		 */
		__initTemplate	: function (meta) {
			meta = meta || null;
			this.tpl = new opajs.helpers.tpl({id:'itpl-' + this.getId()});
			if (this.config.position && this.config.raw) {
				this.tpl.setHtml(this.config.raw);
				this.tpl.prepare(meta);
				O.srv('layout').add(this.config.view, this);
				this.__onLoad();
			}
			else { this.onLoad(); }
		},
		
		/**
		 * Load the view widgets
		 * @return	{void} 
		 */
		__loadWidgets	: function () {
			if (this.config.widgets) {
				var self = this, widget, widgets = this.config.widgets;
				for (var i = 0; i < widgets.length; i++) {
					if (!widgets[i].id) { widgets[i].id = O.utils.createId(); }
					if (!O.wdg(widgets[i].id)) {
						widget = new Widgets[widgets[i].widget](widgets[i]);
						O.srv('layout').widgets.set(widgets[i].id, widget);
					}
				}
			}
		},
		
		/**
		 * View preload
		 * @return	{void}
		 */
		__onLoad	: function () {
			this.tpl.setRender(this.config.layout.id);
			this.tpl.render();
			O.srv('layout').activate(this.config.view);
			this.onLoad();
			if (this.config.controller) {
				O.ctl(this.config.controller).onLoad(this.config.position);
			}
		},
		
		/**
		 * View reload
		 * @return	{void} 
		 */
		__onOverload	: function () {
			O.srv('layout').activate(this.config.view);
			this.onOverload();
			O.ctl(this.config.controller).onOverload(this.config.position);
		},
		
		/*
		 * Get el property
		 * @param	{String}
		 * @param	{String}
		 * @return	{String}
		 */
		getProperty: function (id, propertyName) {
			propertyName = propertyName || 'href';
			return (Ext.get(id).dom.attributes[propertyName].value);
		},
		
		/**
		 * Bind a route action
		 * @param	{String}
		 * @param	{String}
		 * @return	{void}
		 */
		route: function (id, property) {
			property = property || 'href';
			var self = this;
			
			var cb = function (e, el) {
				e.preventDefault();
				self.link(self.getProperty(el.id, property));
			};
			
			this.unbind(id, 'click', cb);
			this.bind(id, 'click', cb);
		},
		
		/*
		 * Go to URI
		 * @param	{string}
		 * @return	{void}
		 */
		link: function (uri) {
			var base = O.utils.rtrim(O.srv('routes').cfg.navigation.defaultUri, '/') + '/';
			O.srv('routes').go(base + uri, true);
		},
		
		/**
		 * Execute an API call
		 * @param	{string}
		 * @param	{Object}
		 * @return	{void} 
		 */
		emit : function (api, data) {
			if (this.config.service && App.services[this.config.service]) { emiter = App.services[this.config.service]; }
			else if (this.config.controller && O.ctl(this.config.controller)) { emiter = O.ctl(this.config.controller); }
			else { this.error(translate('OPAJS.view.emit.emiterNotFound')); }
			if (emiter[api]) { emiter[api](data); }
		},
		
		/**
		 * Fill element's inner
		 * @param	{string}
		 * @param	{string}
		 * @return	{void}
		 */
		fill : function (id, str) {
			var el = Ext.get(id); str = str || null;
			if (el) { el.dom.innerHTML = str; }
		},
		
		/**
		 * Fill input value
		 * @param	{string}
		 * @param	{string}
		 * @return	{void}
		 */
		set : function (id, str) {
			var el = Ext.get(id); str = str || '';
			if (el) { el.dom.value = str; }
		},
		
		/**
		 * Edit button
		 * @param	{string}
		 * @param	{string}
		 * @return	{void}
		 * @deprecated
		 */
		button : function (id, color, size, str, icon, iconposition) {
			var _str, el = Ext.get(id); color = color || 'default'; size = size || 'sm'; str = str || ''; icon = icon || ''; iconposition = iconposition || 'right';
			if (el) {
				var colors = ['default', 'infos', 'primary', 'success', 'warning', 'danger', 'dark'];
				var sizes = ['lg', 'md', 'sm', 'xs'];
				el.removeClass('btn');
				
				for (var i = 0; i < colors.length; i++) { el.removeClass('btn-' + colors[i]); }
				for (var i = 0; i < sizes.length; i++) { el.removeClass('btn-' + sizes[i]); }
				
				el.addClass('btn'); el.addClass('btn-' + color); el.addClass('btn-' + size); //el.addClass('bbox');
				
				if (icon && iconposition === 'left') { _str = '<i class="fa fa-' + icon + '"></i> ' + str; }
				else if (icon && iconposition === 'right') { _str = str + ' <i class="fa fa-' + icon + '"></i>'; }
				else { _str = str }
				
				el.dom.innerHTML = _str;
			}
		},

		/**
		 * Edit the messages box
		 * @param	{string}
		 * @param	{string}
		 * @param	{string} in px
		 * @param	{int} in milliseconds
		 * @return	{void}
		 * @deprecated
		 * @todo : Make a service
		 */
		msgb : function (str, icon, color, size, time) {
			str = str || 'Error'; icon = icon || 'times'; bgColor = color || 'red'; size = size || 18; time = time || 3000;
			var id, txtColor = 'white', id = 'messagesBox';

			this.hideMsgb(id);
			this.fill('msgb:text', str);
			this.fill('msgb:icon', '<span class="fa fa-' + icon + '"></span>');
			this.setBackgroundColor(id, bgColor);
			this.setTextColor(id, txtColor);
			this.setTextSize(id, size);
			this.showMsgb(id, time);
		},

		setTextSize : function (id, size) {
			var el = Ext.get(id);
			if (!el){ return; }
			el.setStyle({'font-size':size + 'px'});
		},

		setBackgroundColor : function (id, color) {
			var colors = ['red', 'orange', 'green', 'blue'],
				el = Ext.get(id);
			if (!el){ return; }
			for (var i = 0; i < colors.length; i++) { el.removeClass('bg-' + colors[i]); }
			el.addClass('bg-' + color);
		},

		setTextColor : function (id, color) {
			var colors = ['red', 'orange', 'green', 'blue', 'gray', 'white'],
					el = Ext.get(id);
			if (!el){ return; }
			for (var i = 0; i < colors.length; i++) { el.removeClass('txt-' + colors[i]); }
			el.addClass('txt-' + color);
		},

		setIcon : function (id, remove, by) {
			el = Ext.get(id);
			if (!el){ return; }
			if (el.hasClass(remove)) { el.removeClass(remove); }
			el.addClass(by);
		},

		showMsgb : function (id, time) {
			var self = this, el = Ext.get(id);
			if (!el) { return; }

			el.removeClass('xx-none');
			el.removeClass('xx-hide');
			el.removeClass('fadeOut');

			el.addClass('fadeInDownBig');
			el.addClass('xx-block');
			el.addClass('xx-show');

			if (time > 0) {
				setTimeout(function () { self.hideMsgb(id); }, time); }
		},

		hideMsgb : function (id) {
			var el = Ext.get(id);
			if (!el) { return; }

			el.removeClass('xx-block');
			el.removeClass('xx-show');
			el.removeClass('fadeInDownBig');

			el.addClass('fadeOut');
			setTimeout(function () {
				el.addClass('xx-none');
				el.addClass('xx-hide');
			}, 1000);
		},
		
		/**
		 * Set element HREF
		 * @param	{string}
		 * @param	{string}
		 * @return	{void} 
		 */
		setHref : function (id, link) {
			var el = Ext.get(id); link = link || null;
			if (el) { el.dom.href = link; }
		},

		/**
		 * Set image SRC
		 * @param	{string}
		 * @param	{string}
		 * @return	{void}
		 */
		setSrc : function (id, src) {
			var el = Ext.get(id); src = src || null;
			if (el) { el.dom.src = src; }
		},
		
		/**
		 * Set checkbox status
		 * @param	{string}
		 * @param	{string}
		 * @return	{void} 
		 */
		check : function (id, checked) {
			var el = Ext.get(id);
			if (el) { el.dom.checked = (checked) ? false : true; }
		},
		
		/**
		 * Get checkbox status
		 * @param	{string}
		 * @param	{string}
		 * @return	{void} 
		 */
		isChecked : function (id) {
			var el = Ext.get(id), res = false;
			if (el) { res = (el.dom.checked) ? true : false; }
			return (res);
		},
		
		/**
		 * Get element value
		 * @param	{string}
		 * @return	{string} 
		 */
		get : function (id) {
			var el = Ext.get(id);
			if (el) { return (el.dom.value); }
			return (null);
		},
		
		/**
		 * Get string of [HTMLDivElement]
		 * @param	{HTMLDivElement}
		 * @return	{string} 
		 */
		htmlTOstring : function (node) {
			var str, tmpNode = document.createElement( "div" ); tmpNode.appendChild( node.cloneNode( true ) );
			str = tmpNode.children[0].innerHTML; tmpNode = node = null;
			return (str);
		},
		
		/**
		 * Remove a DOM Element
		 * @param	{HTMLDivElement}
		 * @return	{string} 
		 */
		removeNode : function (id) {
			var el = document.getElementById(id);
			el.parentElement.removeChild(el);
		},
		
		/**
		 * Init the view
		 * @param	{Object}
		 * @return	{void}
		 */
		init	: function () { return; },
		
		/**
		 * Before render 
		 * @return	{void}
		 */
		beforeRender	: function () { return; },
		
		/**
		 * After render
		 * @return	{void}
		 */
		afterRender	: function () { return; },
		
		/**
		 * On load view
		 * @param	{String}
		 * @param	{String}
		 * @return	{void}
		 */
		onLoad	: function (position, overloaded) { return; },
		
		/**
		 * On overload view
		 * @param	{String}
		 * @return	{void}
		 */
		onOverload	: function (position) { return; },
		
		/**
		 * On unload view
		 * @return	{void}
		 */
		unLoad	: function () { return; },
		
		/**
		 * On unload view
		 * @return	{void}
		 */
		onQuit	: function () { return; },
		
		/**
		 * On resized view
		 * @return	{void}
		 */
		onResize	: function () { return; }
	}
);