/**
 * @category	Proxy
 * @package		engine.helpers.proxies
 * @namaspace	opajs.helpers.proxies
 */

Ext.ns('opajs.helpers.proxies');

opajs.helpers.proxies.message = Ext.extend(opajs.helpers.proxies.iproxy, {

		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.proxies.message}
		 */
		constructor : function (config) {
			Ext.apply(this.cfg, config);
			opajs.helpers.proxies.message.superclass.constructor.apply(this);
			this.events = null; this.message = {}; this.result = {meta: new opajs.helpers.store(), records: new opajs.helpers.store(), error: null};
		},
		
		/**
		 * Launch a message request
		 * @param	{Object}
		 * @param	{Function}
		 * @return	{void}
		 */
		request: function (config, callback, id) {
			var self = this; id = id || O.utils.createId();
			if (!config.params) { config.params = ''; }
			Ext.apply(this.cfg, config);
			this.setId(id);
			O.srv('bus').send(this.encodeParams());
			this.callback = null;
			if (callback) { this.callback = callback; }
		},
		
		/**
		 * Prepare parameters format to send
		 * @return	{Object} 
		 */
		encodeParams	: function () {
			var obj = { uri : this.cfg.uri, data : this.cfg.params, requestId : this.getId(), token: O.srv('auth').getToken() };
			return (JSON.stringify(obj));
		},
		
		/**
		 * Set message
		 * @param	{object}
		 * @param	{object}
		 * @return	{void} 
		 */
		setMessage	: function (event, message) {
			this.event = event; this.message = message; this.result.meta.sets(this.message.meta); this.result.records.sets(this.message.records);
			var obj = (this.message.meta && this.message.meta.rted && this.message.meta.rted.obj) ? this.message.meta.rted.obj : null;
			var callback = (this.message.meta && this.message.meta.rted && this.message.meta.rted.callback) ? this.message.meta.rted.callback : null;
			if (this.callback && !callback) { return (this.callback(this.result.meta, this.result.records, this.result.error, message, event, this.getId())); }
			if (!obj && callback) {
				return (callback(this.result.meta, this.result.records, this.result.error, message, event, this.getId()));
			}
			else if (obj && callback) {
				if (App.rt[obj] && App.rt[obj][callback]) {
					return (App.rt[obj][callback](this.result.meta, this.result.records, this.result.error, message, event, this.getId()));
				}
				warn('RealTime callback not found - ' + obj + '::' + callback);
				return (callback(this.callback(this.result.meta, this.result.records, this.result.error, message, event, this.getId())));
			}
		}
	}
);