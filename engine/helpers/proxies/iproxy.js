/**
 * @category	Proxy
 * @package		engine.helpers.proxies
 * @namaspace	opajs.helpers.proxies
 */

Ext.ns('opajs.helpers.proxies');

opajs.helpers.proxies.iproxy = Ext.extend(opajs.helpers.model, {

		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.proxies.iproxy}
		 */
		constructor : function (config) {
			Ext.apply(this.cfg, config);
			this.raw = null;
			opajs.helpers.proxies.iproxy.superclass.constructor.apply(this);
		},
		
		/**
		 * Launch a request
		 * @param	{Object}
		 * @param	{Function}
		 * @return	{void}
		 */
		request: function (config, callback) { return; },
		
		/**
		 * After request method
		 * @param	{Object}
		 * @param	{Object}
		 * @param	{Function}
		 * @return	{void}
		 */
		afterRequest	: function (config, resObj, callback) {
			if(this.cfg.local){if(resObj && resObj.responseText){ localStorage.setItem(this.getId() + '_' + config.uri, resObj.responseText); }}
	    	this.meta.clean(); this.records.clean(); this.error();
	    	this.raw = resObj.responseText;
	    	
	    	if (!this.cfg.noDecode && resObj && resObj.responseText) {
	    		var result = {};
				try {
					result = JSON.parse(resObj.responseText);
					if (result.meta) { this.meta.sets(result.meta); }
					else {
						this.meta.sets(result);
					}
					if (result.records) { this.records.sets(result.records); }
					this.error(result.error);
				}
				catch (e) { result.error = e; }
				if (result.error != null) {
					this.error(translate('proxy.reponse.server.internalError') + '::' + result.error.message);
					if (this.cfg.onError) {
						this.cfg.onError(this.getLastError(), this.raw, resObj, config);
						return;
					}
				}
	    	}
	    	
	    	if (callback && this.cfg.scope) { callback = this.cfg.scope.callback; }
			callback(this.meta, this.records, this.getLastError(), this.raw, resObj, config);
		},
		
		/**
		 * Generate the request uri
		 * @return	{string}
		 */
		getURI : function () {
			var res;
			if (!this.cfg.ref) { this.cfg.ref = 'def'; }
			var route = Config.routes.callers[this.cfg.ref];
			if (!route) {
				this.cfg.ref = 'def';
				route = Config.routes.callers[this.cfg.ref];
			}
			res = route.protocol + '://' + route.domain + '/' + O.utils.trim(this.cfg.uri, '/');
			return (res);
		},
		
		/**
		 * Prepare parameters format to send
		 * @return	{Object} 
		 */
		encodeParams	: function () {
			var params = (this.cfg.params && this.cfg.noEncode) ? this.cfg.params : {input:JSON.stringify(this.cfg.params)};
			return (params);
		}
	}
);