/**
 * @category	Proxy
 * @package		engine.helpers.proxies
 * @namaspace	opajs.helpers.proxies
 */

Ext.ns('opajs.helpers.proxies');

opajs.helpers.proxies.request = Ext.extend(opajs.helpers.proxies.iproxy, {

		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.proxies.request}
		 */
		constructor : function (config) {
			Ext.apply(this.cfg, config);
			opajs.helpers.proxies.request.superclass.constructor.apply(this);
		},
		
		/**
		 * Launch a XHR request
		 * @param	{Object}
		 * @param	{Function}
		 * @return	{void}
		 */
		request: function (config, callback) {
			var self = this;
			if (!config.params) { config.params = ''; }
			Ext.apply(this.cfg, config);
			Ext.Ajax.request ({
					timeout : this.getTimeout(),
				    url		: this.getURI() + this.getTokenURI(),
					method	: this.getMethod(),
					params	: this.encodeParams(),
				    success	: function(resObj) { self.afterRequest(config, resObj, callback); },
					failure: function(resObj) {
						if (self.cfg.onError) { self.cfg.onError(config, resObj); }
						else {
							self.error('JS.xhr.requestFailure');
							//ln(resObj.statusText);
						}
					}
				}
			);
		},
		
		/**
		 * Generate the token request
		 * @return	{String}
		 */
		getTokenURI : function () {
			var d = new Date(), res = '/?t=' + d.getTime(),
			res = res + '&token=' + opajs.services.auth.getToken();
			return (res);
		},
		
		/**
		 * Get the request method
		 * @return	{string}
		 */
		getMethod : function () {
			var res = 'GET';
			if (this.cfg.params) { res = 'POST'; }
			if (this.cfg.method) { res = this.cfg.method; }
			return (res);
		},
		
		/**
		 * Get the request timeout
		 * @return	{string}
		 */
		getTimeout : function () {
			var res = 600000;
			if (this.cfg.timeout) { res = this.cfg.timeout; }
			return (res);
		}
	}
);