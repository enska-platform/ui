/**
 * @category	Helper
 * @package		engine.helpers
 * @namaspace	opajs.helpers
 */

Ext.ns('opajs.helpers');
Ext.ns('App.services');

opajs.helpers.controller = Ext.extend(opajs.helpers.model, {
		
		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.controller}
		 */
		constructor : function (config) {
			this.overloaded = false;
			if (config) { Ext.apply(this, config); }
			opajs.helpers.controller.superclass.constructor.apply(this);
			this.setId(opajs.services.routes.controller);
		},
		
		/**
		 * Receive an internal notification (from OPAJS core)
		 * @param	{Object}
		 */
		onEvent	: function (event) { return; },
		
		/**
		 * Receive an external notification (from websocket, ...)
		 * @param	{Object}  
		 */
		onPush	: function (event) { return; },
		
		/**
		 * On load
		 * @param	{string}
		 * @return	{void}
		 */
		onLoad	: function (position) { return; },
		
		/**
		 * On overload
		 * @param	{string}
		 * @return	{void}
		 */
		onOverload	: function (position) { return; },
		
		logout	: function (data) {
			O.srv('auth').refreshToken();
			O.srv('routes').loop();
		},
		
		/**
		 * Init controller's instances
		 * @param	{Bool}
		 * @return	{void} 
		 */
		__init		: function (overloaded) {
			if (typeof(this.services) == 'object' && !overloaded) {
				for (var i=0; i < this.services.length; i++) {
					this.__loadService(this.services[i]);
				}
			}
			var self = this;
			setTimeout(function (){
				if (typeof(self.config) == 'object') {
					for (var i in self.config) {
						if (overloaded) { self.__overload(i) }
						else { self.__load(i); }
					}
					O.srv('layout').display(self.config);
				}
			}, 200);
		},
		
		/**
		 * Load service
		 * @param	{String} Service name
		 * @return	{void} 
		 */
		__loadService	: function (service) {
			if (O.srv(service)) { return; }
			opajs.services.services.include(service, function (service) {
				setTimeout(function (){
					var obj = {id: service, instance: App.services[service]};
					if (obj.instance && obj.instance.onLoad) { obj.instance.onLoad(); }
				}, 100);
			} , 'app');
		},
		
		/**
		 * Load view template
		 * @param	{String}	viewport position (top, left, right, footer)
		 * @return	{void} 
		 */
		__load		: function (position) {
			var instance, self = this;
			var callbackTemplate = function (meta, records, error, raw, resObj, config) {
				if (!error) {
					var view = self.config[config.position].tpl.view;
					if (App.views[view]) { new App.views[view]({id:config.id, config:{raw:raw, position:config.position, view:view, controller: self.getId()}}); }
					else { self.error('OPAJS.controller.__load.viewNotExists'); self.error(self.getLastError() + '::' + view);}
				}
				else { self.error('OPAJS.controller.__load.request.template'); self.error(self.getLastError() + '::' + error); }
			};
			
			if (this.config[position].tpl) {
				var view = (this.config[position].view) ? this.config[position].view : position;
				if (O.vw(view)) { O.vw(view).__onOverload(); return; }
				this.config[position].tpl.noDecode = true;
				this.config[position].tpl.ref = 'tpl';
				this.config[position].tpl.position = position;
				this.config[position].tpl.view = view;
				this.config[position].tpl.ctl = this.getId();
				if (this.config[position].tpl.remote) {
					return (this.request(this.config[position].tpl, callbackTemplate));
				}
				this.display(this.config[position].tpl, callbackTemplate);
			}
		},
		
		/**
		 * Display local template
		 * @param	{Object}	request config
		 * @param	{Object} 	callback
		 * @return	{void}
		 */
		display	: function (config, callback)
		{
			var tpl = null, error = null;
			if (!config.uri) { error = 'TPL uri not found'; }
			else {
				tpl = O.tpl(config.uri);
				if (!tpl) { error = ' TPL not found :: ' + config.uri; }
				else { tpl = tpl.innerHTML; }
			}
			callback(null, null, error, tpl, null, config);
		},
		
		/**
		 * Overload view template
		 * @param	{String}	viewport position (top, left, right, footer)
		 * @return	{void} 
		 */
		__overload		: function (position) {
			if (this.config[position].tpl) {
				var view = (this.config[position].view) ? this.config[position].view : position;				
				if (O.vw(view)) { O.vw(view).__onOverload(); return; }
			}
		},

		/**
		 * 	load a controller sub-section
		 * @param	{string}
		 * @param	{string}
		 * @return	{void}
		 */
		sectionLoad : function (application, section) {
			O.srv('layout').viewport.onResizeBC();
			application = application || O.srv('routes').spn[0] || 'index';
			section = section || O.srv('routes').spn[1] || 'index';
			O.vw(application).load(section);
		}
	}
);