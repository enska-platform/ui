/**
 * @category	Helper
 * @package		engine.helpers
 * @namaspace	opajs.helpers
 */

Ext.ns('opajs.helpers');

opajs.helpers.viewport = Ext.extend(opajs.helpers.model, {
		
		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.viewport}
		 */
		constructor : function (config) {	
			if (config) { Ext.apply(this, config); }
			opajs.helpers.model.superclass.constructor.apply(this);
			this.__init();
		},
		
		/**
		 * Init the parent viewport
		 * @return	{void} 
		 */
		__init	: function () {
			var tpls = Ext.get('app:templates');
			this.dom = {
				html	: Ext.get('html-tag'),
				head	: Ext.get('head-tag'),
				title	: Ext.get('title-tag'),
				body	: Ext.get('body-tag'),
				tpl		: new opajs.helpers.store()
			};
			
			if (tpls) {
				for (var i = 0; i < tpls.dom.children.length; i++) {
					this.dom.tpl.set(tpls.dom.children[i].id, tpls.dom.children[i]);
				}
			}
			
			this.tpl = {};
			this.elems = {};
			this.tpl.main = {tag:'section', id:'mainRender', cls: 'vbox'};
			this.dom.html.addClass('opajs_V0-2-1');
			this.init();
		},
		
		/**
		 * Render the viewport
		 * @return	{void} 
		 */
		render	: function () {
			this.clear();
		},
		
		/**
		 * Clear the viewport
		 * @return	{void} 
		 */
		clear	: function () {
			this.elems.main = Ext.DomHelper.overwrite(this.dom.body, this.tpl.main);
		},
		
		/**
		 * Get a template
		 * @param	{String}
		 * @return	{string} 
		 */
		getTpl	: function (tplName) {
			return (this.dom.tpl.get(tplName));
		},
		
		/**
		 * Init the viewport
		 * @return	{void} 
		 */
		init	: function () { return; },
		
		/**
		 * Add a view to catalog
		 * @param	{Object}
		 * @return	{void} 
		 */
		addView	: function (config) { return; },
		
		/**
		 * Activate a specific view
		 * @param	{String}
		 * @return	{void} 
		 */
		activate	: function (view) { return; },
		
		/**
		 * Desactivate a specific view
		 * @param	{String}
		 * @return	{void} 
		 */
		desactivate	: function (view) { return; },
		
		/**
		 * Display a specific container
		 * @param	{String}
		 * @param	{Bool} = true
		 * @return	{void} 
		 */
		display	: function (container, displayed) { return; }
	}
);