/**
 * @category	Helper
 * @package		engine.helpers
 * @namaspace	opajs.helpers
 */

Ext.ns('opajs.helpers');

opajs.helpers.model = Ext.extend(function () {}, {
		
		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.model}
		 */
		constructor : function (config) {
			if (config) { Ext.apply(this, config); }
			opajs.helpers.model.superclass.constructor.apply(this);
			this._error = null;
			this.cb = {};
			this.id = this.id || this.setId(O.utils.createId());
			this.cfg = this.cfg || {};
			this.meta = this.meta || new opajs.helpers.store();
			this.records = this.records || new opajs.helpers.store();
			this.config = this.config || {};
		},
		
		/**
		 * Get meta
		 * @return	{opajs.helpers.stores.leaf}
		 */
		getMeta		: function () {
			return (this.meta);
		},
		
		/**
		 * Get records
		 * @return	{opajs.helpers.stores.node}
		 */
		getRecords	: function () {
			return (this.records);
		},
		
		/**
		 * Setter for modelID
		 * @param	{String}
		 * @return	{void}
		 */
		setId : function (id) {
			this.id = id;
		},
		
		/**
		 * Getter for modelID
		 * @return	{String}
		 */
		getId : function () {
			return (this.id);
		},
		
		/**
		 * Set the latest error
		 * @param	{string}
		 * @return	{void} 
		 */
		error: function (error) {
			this._error = translate(error);
			if (this._error) { console.warn(this._error); }
		},
		
		/**
		 * Get the latest error
		 * @return	{string} 
		 */
		getLastError: function () {
			return (this._error);
		},
		
		/**
		 * Launch a remote request
		 * @param	{Object}
		 * @param	{mixed}
		 * @param	{Bool}
		 * @return	{void}
		 */
		request: function (config, callback, id) {
			id = id || this.getId();
			this.cfg.proxy = this.cfg.proxy || {};
			this.cfg.proxy.type = ((this.cfg.proxy && this.cfg.proxy.type) ? this.cfg.proxy.type : (Config.proxies && Config.proxies.def) ? Config.proxies.def : 'request'); 
			return ((this.cfg.proxy.type === 'message') ? this.__message(config, callback, id) : (this.cfg.proxy.type === 'request') ? this.__request(config, callback, id) : this.error('OPJS.proxies.typeNotFount'));
		},
		
		getURI : function (config) {
			var res;
			if (!config.ref) { config.ref = 'def'; }
			var route = Config.routes.callers[config.ref];
			if (!route) {
				config.ref = 'def';
				route = Config.routes.callers[config.ref];
			}
			res = route.protocol + '://' + route.domain + '/' + O.utils.trim(config.uri, '/');
			return (res);
		},
		 
		/**
		 * Bind event action controls
		 * @param	{String}	id
		 * @param	{String} 	event
		 * @param	{String} 	callback
		 * @param	{Object} 	params
		 * @param	{bool}		isUnbind
		 * @return	{void}
		 */		
		bind: function (id, e, callback, params, isUnbind) {
			params = params || {};
			var _el = _els = null
			this.cb = this.cb || {};
			if (Ext.isString(id)) { this.cb[id] = callback; }
			
			if (Ext.isString(id) && id.substr(0,1) == '<') { _els = Ext.select(id.substr(1, id.length - 1)); }
			else if (Ext.isString(id) && id.substr(0,1) == '.') { _els = Ext.query(id); }
			else { _el = Ext.get(id); }
			
			if (_els && _els !== 'undefined') {
				for (var i = 0; i < _els.length; i++) {
					_els[i] = Ext.get(_els[i]);
					if (isUnbind) { _els[i].un(e, callback, this, params); }
					else { _els[i].on(e, callback, this, params); }
				}
			}
			else if (_el && _el !== 'undefined') {
				if (isUnbind) { _el.un(e, callback, this, params); }
				else { _el.on(e, callback, this, params); }
			}
		},
		
		/**
		 * Unbind event action controls
		 * @param	{String} id
		 * @param	{String} event
		 * @param	{String} callback
		 * @return	{void}
		 */		
		unbind: function (id, e, callback, params) {
			this.bind(id, e, callback, params, true);
		},
		
		/**
		 * Launch a XHRRequest
		 * @param	{Object}
		 * @param	{mixed}
		 * @param	{Bool}
		 * @return	{String}
		 */
		__request: function (config, callback, id) {
			return (O.srv('proxy').request(config, callback, id));
		},
		
		/**
		 * Launch RTMessage
		 * @param	{Object}
		 * @param	{mixed}
		 * @param	{Bool}
		 * @return	{String}
		 */
		__message: function (config, callback, id) {
			return (O.srv('proxy').message(config, callback, id));
		}
	}
);