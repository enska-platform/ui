/**
 * @category	Helper
 * @package		engine.helpers
 * @namaspace	opajs.helpers
 */

Ext.ns('opajs.helpers');

opajs.helpers.tpl = Ext.extend(opajs.helpers.model, {
		
		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.tpl}
		 */
		constructor : function (config) {
			if (config) { Ext.apply(this, config); }
			opajs.helpers.tpl.superclass.constructor.apply(this);
			this.__init();
			this.init();
		},
		
		/**
		 * Init the template
		 * @return	{void} 
		 */
		__init : function () {
			this.cfg = this.cfg || {};
			this.tpl = new Ext.Template();
			this.raw = this.cfg.raw || null;
			this.html = this.cfg.html || null;
			this.elem = null;
			this.renderTo = this.cfg.renderTo || null;
			this.setHtml();
			
			if (this.cfg.meta) { this.tpl.set(this.prepare(this.cfg.meta)); }
		},
		
		/**
		 * Set the HTML template raw
		 * @param	{String}	html
		 * @return	{void}
		 */
		setHtml		: function (html) {
			if (this.raw && !html) { 
				this.html = O.srv('layout').translate(this.raw) ; 
				this.tpl.set(this.html);
			}
			else if (html) {
				this.raw = html; 
				this.html = O.srv('layout').translate(html);
				this.tpl.set(this.html);
			}
		},
		
		/**
		 * Prepare the template
		 * @param	{mixed}
		 * @return 	{String}
		 */
		prepare		: function (meta) {
			if (meta && typeof(meta) === 'object') {
				this.html = this.tpl.apply(meta);
				return	(this.html);
			}
		},
		
		/**
		 * Custom init call
		 * @return	{void} 
		 */
		init	: function () { return ; },
		
		/**
		 * Get template's raw html
		 * @return	{String}
		 */
		getRaw	: function () {
			return (this.raw);
		},
		
		/**
		 * Get templated html
		 * @return	{String}
		 */
		getHTML	: function () {
			return (this.html);
		},
		
		/**
		 * Set the render id
		 * @pram	{String}
		 * @return	{void} 
		 */
		setRender	: function (id) {
			this.renderTo = id;
		},
		
		/**
		 * Render the TPL
		 * @param	{String}
		 * @return	{void} 
		 * @todo	this.tpl.overwrite(id); ( /!\ bug into extcore310 with tag {T} ... Try to do not request applyTemplate() )
		 */
		render	: function (id) {
			id = id || this.renderTo || null;
			var el = Ext.fly(id);
			if (el) { el.dom.innerHTML = this.html; }
		}
	}
);