	/**
 * @category	Helper
 * @package		engine.helpers.viewports
 * @namaspace	opajs.helpers.viewports
 */

Ext.ns('opajs.helpers.viewports');

opajs.helpers.viewports.themekit = Ext.extend(opajs.helpers.viewport, {
		
		/**
		 * Themkit class
		 * @type	{Object} 
		 */
		adapters	: {
			top		: 'ls-top-navbar',
			footer	: 'ls-bottom-footer',
			left	: 'sidebar-l2',
			right	: 'sidebar-r1',
			content	: ''
		},
		
		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.viewport.themekit}
		 */
		constructor : function (config) {
			if (config) { Ext.apply(this, config); }
			opajs.helpers.viewports.themekit.superclass.constructor.apply(this);
		},
		
		/**
		 * Init the viewport
		 * @return	{void} 
		 */
		init	: function () {
			this.tpl.nav = {tag: 'div', id: 'topRender', cls: 'xx-hide xx-none'};
			
			/*this.tpl.sidesbars = {
				tag: 'div', id: 'sidesbarsRender', cls:'st-container', children: [
					{tag: 'div', id: 'leftRender', cls: 'xx-hide xx-none'},
					{tag: 'div', id: 'rightRender', cls: 'xx-hide xx-none'}
				]
			};*/
			
			this.tpl.sidesbars = {
				tag: 'div', id: 'leftRender', cls:'st-container'
			};
			
			this.tpl.content = {
				tag: 'div', id: 'content', children:[
					{tag: 'div', id: 'contentRender', class: 'xx-hide xx-none container-fluid st-layout'}
				]
			}
			this.tpl.footer = {tag: 'div', id: 'footerRender', cls: 'xx-hide xx-none'};
			this.dom.html.addClass('themekit st-layout');
		},
		
		/**
		 * Render the viewport
		 * @return	{void} 
		 */
		render	: function () {
			this.clear(); this.elems.contents = {};
			this.elems.nav = Ext.DomHelper.append(this.elems.main, this.tpl.nav);
			this.elems.sidesbar = Ext.DomHelper.append(this.elems.main, this.tpl.sidesbars);
			this.elems.content = Ext.DomHelper.append(this.elems.sidesbar, this.tpl.content);
			this.elems.footer = Ext.DomHelper.append(this.elems.main, this.tpl.footer);
		},
		
		/**
		 * Activate a specific view
		 * @param	{String}
		 * @return	{void} 
		 */
		activate	: function (view) {
			this.__activate(view, true);
		},
		
		/**
		 * Desactivate a specific view
		 * @param	{String}
		 * @return	{void} 
		 */
		desactivate	: function (view) {
			this.__activate(view, false);
		},
		
		/**
		 * Refresh viewport
		 * @param	{String}
		 * @return	{void} 
		 */
		display	: function (config) {
			this.resetDisplay();
			this.resetAdapters();
			for (var i in config) {
				this.__adapte(i, true);
				this.__display(i, true);
			}
		},
		
		/**
		 * Show a specific container
		 * @param	{String}
		 * @return	{void} 
		 */
		showContainer	: function (container) {
			O.utils.show(container + 'Render');
		},
		
		/**
		 * Hide a specific container
		 * @param	{String}
		 * @return	{void} 
		 */
		hideContainer	: function (container) {
			O.utils.hide(container + 'Render');
		},
		
		/**
		 * Add a specific adapter
		 * @param	{String}
		 * @return	{void} 
		 */
		addAdapter	: function (adapter) {
			if (adapter === 'left' || adapter === 'right') {
				if (this.dom.html.hasClass('hide-sidebar')) { !this.dom.html.removeClass('hide-sidebar') }
				if (!this.dom.html.hasClass('sidebar-l3')) { !this.dom.html.addClass('sidebar-l3') }
				if (!this.dom.html.hasClass('show-sidebar')) { !this.dom.html.addClass('show-sidebar') }
				return;
			}
			adapter = this.adapters[adapter];
			if (!this.dom.html.hasClass(adapter)) { this.dom.html.addClass(adapter); }
		},
		
		/**
		 * Remove a specific adapter
		 * @param	{String}
		 * @return	{void} 
		 */
		remAdapter	: function (adapter) {
			if (adapter === 'left' || adapter === 'right') {
				if (this.dom.html.hasClass('show-sidebar')) { !this.dom.html.removeClass('show-sidebar') }
				if (this.dom.html.hasClass('sidebar-l3')) { !this.dom.html.removeClass('sidebar-l3') }
				if (!this.dom.html.hasClass('hide-sidebar')) { !this.dom.html.addClass('hide-sidebar') }
				return;
			}
			adapter = this.adapters[adapter];
			if (this.dom.html.hasClass(adapter)) { this.dom.html.removeClass(adapter); }
		},
		
		/**
		 * Hide all containers
		 * @return	{void} 
		 */
		resetDisplay	: function () {
			var items = ['top', 'footer', 'content', 'left', 'right'];
			for (var i = 0; i < items.length ; i++) { this.hideContainer(items[i]); }
		},
		
		/**
		 * Remove all adapters
		 * @return	{void} 
		 */
		resetAdapters	: function () {
			var items = ['top', 'footer', 'content', 'left', 'right'];
			for (var i = 0; i < items.length ; i++) { this.remAdapter(items[i]); }
		},
		
		/**
		 * Add a view to catalog
		 * @param	{Object}
		 * @return	{bool}
		 */
		addView	: function (config) {
			var render, tpl, view = O.vw(config);
			if (view) {
				render = view.config.position || 'content';
				tpl = {id : view.tpl.getId(), tag : 'div', cls : 'xx-hide xx-none', children : []};
				view.config.layout = tpl;
				if (this.elems.contents[view.tpl.getId()]) { this.elems.contents[view.tpl.getId()].remove(); }
				this.elems.contents[view.tpl.getId()] = Ext.DomHelper.append(render + 'Render', tpl);
				O.srv('layout').views.set(config, view);
				return (true);
			}
			return (false);
		},
		
		/**
		 * Display a specific container
		 * @param	{String}
		 * @param	{Bool} = true
		 * @return	{void} 
		 */
		__display	: function (container, displayed) {
			if (container) { var res = (displayed === true) ? this.showContainer(container) : this.hideContainer(container); }
		},
		
		/**
		 * Adapte the viewport
		 * @param	{String}
		 * @param	{Bool} = true
		 * @return	{void} 
		 */
		__adapte	: function (container, displayed) {
			if (container) { var res = (displayed === true) ? this.addAdapter(container) : this.remAdapter(container); }
		},
		
		/**
		 * Apply activate / desactivate view
		 * @param	{Object}
		 * @param	{Bool}
		 * @return	{Bool} 
		 */
		__activate : function (view, activated) {
			if (!(view = O.vw(view))) { return (false); }
			var el; this.__desactivate(view.config.position);
			if (activated === true) { O.utils.show(this.elems.contents[view.tpl.getId()]); }
		},
		
		/**
		 * Desactivate all views from position
		 * @param	{String}
		 * @return	{void}
		 */
		__desactivate : function (position) {
			O.srv('layout').views.each(function(view) {
				if (view.config && view.config.position && view.config.position === position) { O.utils.hide(view.tpl.id); }
			});
		},
	}
);