/**
 * @category	Helper
 * @package		engine.helpers
 * @namaspace	opajs.helpers
 */

Ext.ns('opajs.helpers');

opajs.helpers.store = Ext.extend(function () {}, {
		
		/**
		 * Constructor
		 * @param	{Object}
		 * @return	{opajs.helpers.store}
		 */
		constructor : function (config) {
			this.cfg = {};
			this.data = {};
			this.carbage = [];
			this._error = null;
			opajs.helpers.store.superclass.constructor.apply(this);
			
			if (config && (!config.data && !config.cfg)) {
				if (typeof(config) === 'object') { this.sets(config); }
				else if (typeof(config) === 'string') {
					try { this.data = this.sets(JSON.parse(config)); }
					catch (e) { this.error(translate('OPAJS.helpers.store.formatError') + '::' + e.message); return; }
				}
			}
			else if(config && (config.data || config.cfg)) { Ext.apply(this, config); this.sets(config.data); }
			if (this.cfg.id) { this.setId(this.cfg.id); }
			if (!this.cfg.proxy) { this.cfg.proxy = {}; }
			if (this.cfg.isStatic === true) { this.load(); }
		},
		
		/**
		 * Read all store's entries with callback
		 * @param	{Object}
		 * @return	{void}
		 */
		each : function (callback, _callback) {
			for (var i in this.data) {
				if (this.data[i] && typeof(this.data[i]) !== 'function') { callback(this.data[i]); }
			}
			if (_callback) { _callback(); }
		},
		
		/**
		 * Count store items
		 * @return	{int}
		 */
		count 		: function () {
			var res = 0;
			if (typeof(this.data) === 'object') {
				for (var i in this.data) { res++; }
			}
			return (res);
		},
		
		/**
		 * Insert item to store
		 * @param	{string}
		 * @param	{mixed}
		 * @return	{void}
		 */
		set			: function (key, value) {
			if (!Ext.isString(key)) { return (this); }
			this.data[key] = value;
			key = '__' + key;
			this[key] = value;
			return (this);
		},
		
		/**
		 * Push item to store
		 * @param	{string}
		 * @param	{mixed}
		 * @return	{void}
		 */
		push			: function (value) {
			if (!key) { return (this); }
			this.carbage.push(value);
			return (this);
		},
		
		/**
		 * Inserts to store
		 * @param	{Object}
		 * @param	{bool}
		 * @return	{void}
		 */
		sets 		: function (items, erase) {
			if (erase === true) { this.clean(); }
			if (typeof(items) === 'string') { items = JSON.parse(items); this.__format(items); }
			else if (typeof(items) === 'function') { this.__format(items.gets()); }
			else if (typeof(items) === 'object') { this.__format(items); }
		},
		
		/**
		 * Get record from specific id
		 * @param	{mixed}	key
		 * @return	{mixed}
		 */
		get 		: function (key) {
			var res = null;
			if (Ext.isString(key) || key >= 0) {
				if (this.data[key]) { res = this.data[key]; }
			}
			return (res);
		},
		
		/**
		 * Get all records over node casting
		 * @param	{Bool}
		 * @param	{Bool}
		 * @return	{mixed}
		 */
		gets 		: function (jsEncode, first) {
			jsEncode = jsEncode || false;
			first = first || true;
			var res = {};
			
			for (var i in this.data) {
				res[i] = (typeof(this.data[i]) === 'object' && typeof(this.data[i].gets) === 'function') ? this.data[i].gets(false, false) : this.data[i]; }
			
			if (first === true && jsEncode === true) { res = JSON.stringify(res); }
			return(res);
		},
		
		/**
		 * Delete a specific record from store
		 * @param	{mixed}
		 * @return	{void}
		 */
		del 		: function (key) {
			if (key && this.data[key]) {
				this.data[key] = null;
				delete(this.data[key]);
			}
		},
		
		/**
		 * Clean the store
		 * @return	{void}
		 */
		clean	: function () {
			delete(this.data);
			this.data = {};
		},
		
		/**
		 * Load a static object
		 * @return	{void} 
		 */
		load	: function (){
			if (this.__checkStorage()) {
				var item = localStorage.getItem(this.getId());
				if (item) { this.sets(item); }
			}
		},
		
		/**
		 * Save a static object
		 * @return	{void} 
		 */
		save	: function (){
			if (this.__checkStorage()) {
				localStorage.setItem(this.getId(), this.gets(true));
				return (this.getId());
			}
			return (null);
		},
		
		/**
		 * Remove a static object
		 * @return	{void} 
		 */
		remove	: function (){
			if (this.__checkStorage()) {
				localStorage.removeItem(this.getId());
			}
		},
		
		/**
		 * Launch a remote request
		 * @param	{Object}
		 * @param	{mixed}
		 * @param	{Bool}
		 * @return	{void}
		 */
		request: function (config, callback, id) {
			id = id || this.getId(); var v;
			this.cfg.proxy = this.cfg.proxy || {};
			this.cfg.proxy.type = ((this.cfg.proxy && this.cfg.proxy.type) ? this.cfg.proxy.type : (Config.proxies && Config.proxies.def) ? Config.proxies.def : 'request'); 
			return ((this.cfg.proxy.type === 'message') ? this.__message(config, callback, id) : (this.cfg.proxy.type === 'request') ? this.__request(config, callback, id) : this.error('OPJS.proxies.typeNotFount'));
		},
		
		/**
		 * Setter for modelID
		 * @param	{String}
		 * @return	{void}
		 */
		setId : function (id) {
			this.id = id;
		},
		
		/**
		 * Getter for modelID
		 * @return	{String}
		 */
		getId : function () {
			return (this.id);
		},
		
		/**
		 * Set the latest error
		 * @param	{string}
		 * @return	{void} 
		 */
		error: function (error) {
			this._error = translate(error);
			console.warn(error);
		},
		
		/**
		 * Get the latest error
		 * @return	{string} 
		 */
		getLastError: function () {
			return (this._error);
		},
		
		/**
		 * Check if browser is loaclStorage compliant
		 * @return	{Bool} 
		 */
		__checkStorage	: function () {
			if (!localStorage || (typeof(localStorage) !== 'object')) {
				this.error('HTML5.localStorage.notSupported');
				return(false);
			}
			return(true);
		},
		
		/**
		 * Format data on recursif schema
		 * @param	{Object}
		 * @return	{void}
		 */
		__format		: function (data) {
			if (typeof(data) === 'object') {
				for (var i in data) {
					if (typeof(data[i]) === 'object' && O.utils.count(data[i]) > 0) { this.data[i] = new opajs.helpers.store(data[i]); }
					else if (typeof(data[i]) !== 'object') { this.data[i] = data[i]; }
				}
			}
		},
		
		/**
		 * Launch a XHRRequest
		 * @param	{Object}
		 * @param	{mixed}
		 * @param	{Bool}
		 * @return	{String}
		 */
		__request: function (config, callback, id) {
			return (O.srv('proxy').request(config, callback, id));
		},
		
		/**
		 * Launch a RTMessage
		 * @param	{Object}
		 * @param	{mixed}
		 * @param	{Bool}
		 * @return	{String}
		 */
		__message: function (config, callback, id) {
			return (O.srv('proxy').message(config, callback, id));
		},
	}
);