/**
 * @category	Service
 * @package		engine.services
 * @namaspace	opajs
 * @namaspace	opajs.services
 */

Ext.ns('opajs.services');

opajs.utils = opajs.services.utils = {
	
	/**
	 * Load the utils service
	 * @return	{void}
	 */
	load: function () {
		if (Config.utils) { Ext.apply(this.cfg, Config.utils); }
	},
	
	/**
	 * Access service API
	 * @return	{this} 
	 */
	access	: function () { return (this); },
	
	/**
	 * Get a string length
	 * @param	{string}  str
	 * @return	{int}
	 */
	strlen: function (str)  {
		var i = 0, res = 0;
		if (!str) { return (res); }
		while (str[i++]) { res++; }
		return res;
	},

	dateDiff : function (date1, date2){
		var tmp = date2 - date1,
			diff = {};

		//Seconds
		tmp = Math.floor(tmp/1000);
		diff.sec = tmp % 60;

		// Minutes
		tmp = Math.floor((tmp-diff.sec)/60);
		diff.min = tmp % 60;

		// Hours
		tmp = Math.floor((tmp-diff.min)/60);
		diff.hour = tmp % 24;

		// Days
		tmp = Math.floor((tmp-diff.hour)/24);
		diff.day = tmp;

		return diff;
	},

	/**
	 * Decode an UT8 string
	 * @param	{string}
	 * @return	{string}
	 */
	utf8_decode : function (str) {
		 return decodeURIComponent(escape(str));
	},
	
	/**
	 * Encode an UT8 string
	 * @param	{string}
	 * @return	{string}
	 */
	utf8_encode : function (str) {
		 return unescape(encodeURIComponent(str));
	},
	
	/**
	 * Include script URL
	 * @param	{String}
	 * @param	{Function}
	 * @param	{String}
	 * @return	{void} 
	 */
	include	: function (url, callback, name) {
		var script = document.createElement('script');
    	script.type = 'text/javascript';
    	script.src = url + '?_ts=' + (new Date().getTime());
    	
    	if (callback) {
	        script.onreadystatechange = callback(name);
	        script.onload = script.onreadystatechange;
	    }
	    
	    document.getElementsByTagName('head')[0].appendChild(script);
	},
	
	/**
	 * Count object items
	 * @param	{Object} obj
	 * @return	{int} 
	 */
	count : function (obj) {
		if (Ext.isObject(obj)) { return (Object.keys(obj).length); }
	},
	
	/**
	 * Trim a string
	 * @param	{string}
	 * @param	{string}  
	 * @return	{string}
	 */
	trim: function (str, chars) {
		return opajs.utils.ltrim(opajs.utils.rtrim(str, chars), chars);
	},
 
 	/**
	 * Trim a string from left
	 * @param	{string}
	 * @param	{string}  
	 * @return	{string}
	 */
	ltrim: function (str, chars) {
		chars = chars || "\\s";
		if (str) { str = str.replace(new RegExp("^[" + chars + "]+", "g"), ""); }
		else { str = '' }
		return (str);
	},
 
 	/**
	 * Trim a string from right
	 * @param	{string}
	 * @param	{string}  
	 * @return	{string}
	 */
	rtrim: function (str, chars) {
		chars = chars || "\\s";
		if (str) { str = str.replace(new RegExp("[" + chars + "]+$", "g"), ""); }
		else { str = '' }
		return (str);
	},
	
	/**
	 * Capitalize the string first letter
	 * @param	{string}
	 * @return	{string}
	 */
	ucfirst: function (str) {
		str += '';
		var f = str.charAt(0).toUpperCase();
		return f + str.substr(1);
	},
	
	strtolower : function (str) {	
		return (str + '').toLowerCase();
	},
	
	/**
	 * Get a microtime now value
	 * @param	{bool} return format
	 * @return	{mixed}
	 */
	microtime: function (get_as_float) {
		var now = new Date().getTime() / 1000;
  		var s = parseInt(now, 10);
  		return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
	},
	
	/**
	 * Generate an unique id
	 * @param	int		size
	 * @return	string	
	 */
	createId : function (size) {
		size = size || 5;
		var id = '';
		for (var i=0; i < size; i++) {
			id += Math.floor((1 + Math.random()) * 0x10000).toString(23).substring(1);
		}
		return (id);
	},
	
	/**
	 * Get a cookie value
	 * @param	{string} cookie name
	 * @return	{mixed}
	 */
	getCookie: function (name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0) === ' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	},

	/**
	 * Set a cookie
	 * @param	{string} cookie name
	 * @param	{string} cookie value
	 * @param	{int} raimaning days 
	 * @return	{void}
	 */
	setCookie: function (attr, value, remember) {
		var domain = Config.routes.navigation.wildcard;
		var date = new Date(); date.setTime(date.getTime() + (opajs.utils._getCookieTime(remember)));
		var expires = "; expires=" + date.toGMTString();
		var cookie = attr + "=" + value + expires + "; domain=." + domain + "; path=/";
		document.cookie = cookie;
	},
	
	/**
	 * Get the configurated cookie time
	 * @param	{book}	remember
	 * @return	{int}
	 */
	_getCookieTime : function (remember)
	{
		var rType = (remember) ? 'auth' : 'pub';
		var timeType = Config.cookies[rType].timeType;
		var cookietime = Config.cookies[rType].cookietime;
		cookietime = (timeType === "d") ? cookietime*24*60*60*1000 : ((timeType === "h") ? cookietime*60*60*1000 : ((timeType === "i") ? cookietime*60*1000 : 0));
		return (cookietime);
	},
	
	/**
	 * Delete a cookie
	 * @param	{string} cookie name
	 * @return	{void}
	 */
	remCookie: function (attr) {
		var domain = Config.routes.navigation.wildcard;
		var date = new Date(); date.setTime(date.getTime() - (this._getCookieTime(true)));
		var expires = "; expires=" + date.toGMTString();
		document.cookie = attr + "=" + '' + expires + "; domain=." + domain + "; path=/";		
	},
	
	/**
	 * Anim and show a specific element
	 * @param	{string} element id
	 * @return	{void}
	 */
	show: function (id) {
		var el = Ext.get(id);
		if (el) {
			if (el.hasClass('xx-hide')) { el.removeClass('xx-hide'); el.addClass('xx-show') }
			if (el.hasClass('xx-none')) { el.removeClass('xx-none'); el.addClass('xx-block') }
		}
	},
	
	/**
	 * Anim and hide a specific element
	 * @param	{string} element id
	 * @return	{void}
	 */
	hide: function (id) {
		var el = Ext.get(id);
		if (el) {
			if (el.hasClass('xx-show')) { el.removeClass('xx-show'); el.addClass('xx-hide'); }
			if (el.hasClass('xx-block')) { el.removeClass('xx-block'); el.addClass('xx-none'); }
		}
	},
	
	__showhide: function (infos) {
		var el = Ext.get(infos.id);
		if (el) {
			el.stopFx().animate({duration:infos.duration, to:{opacity:infos.opacity, display:infos.display}});
		}
	},
	
	getTranslation : function (str, lng) {
		obj = JSON.parse(str);
		if (lng && obj[lng]) { return (obj[lng]); }
		if (Config.i18n.def && obj[Config.i18n.dev]) { return (obj[Config.i18n.dev]); }
	},

	animate	: function (id, animation) {
		var el = Ext.get(id);
		if (el) { el.addClass(animation); }
		else { ln('Error on animate'); }
	}
};
