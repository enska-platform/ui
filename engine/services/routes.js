/**
 * @category	Service
 * @package		engine.services
 */

Ext.ns('opajs.services');

opajs.services.routes = {
	
	/**
	 * Service configuration
	 * @type	{Object} 
	 */
	cfg	: {},
	
	/**
	 * Current uri path
	 * @type	{String} 
	 */
	pathName : null,
	
	/**
	 * Current splitted uri path
	 * @type	{Array} 
	 */
	spn : null,
	
	/**
	 * Current used controller
	 * @type	{String} 
	 */
	controller : null,
	
	/**
	 * Current used application
	 * @type	{String} 
	 */
	application : null,
	
	/**
	 * Current used data
	 * @type	{String} 
	 */
	data : null,
	
	/**
	 * Current loaded controllers
	 * @type	{String} 
	 */
	controllers: [],

	/**
	 * Load the routes service
	 * @return	{void}
	 */
	load: function () {
		this.cfg = this.cfg || {};
		this.setConfig();
		if (Config.routes) { Ext.apply(this.cfg, Config.routes); }
		this.cfg.navigation.defaultUri = '/';
		this.cfg.navigation.controllerIndex = 0;
		this.cfg.navigation.applicationIndex = 1;
		this.cfg.navigation.dataIndex = 2;
	},
	
	setConfig: function() {
		var host = window.location.host.split('.'), cfg = {app:'', sep: '', env:'.'}
		if (host[0]) {
			app = host[0].split('-');
			if (app[0] && app[1]) { cfg.app = app[0]; cfg.env = app[1]; cfg.sep = '-'; }
			else if (app[0] && !app[1]) {
				cfg.app = app[0];
				if (cfg.app === 'dev' || cfg.app === 'stg') {
					cfg.env = ''; cfg.sep = '';
				}
			}
			else { cfg.app = ''; cfg.env = ''; cfg.sep = ''; }
		}
		else { cfg.env = ''; }

		Config.routes.callers.def.domain = cfg.app + cfg.sep + cfg.env + '.' + host[1] + '.' + host[2];
		if (cfg.app === 'dev' || cfg.app === 'stg') {
			cfg.env = cfg.app; cfg.sep = '-';
		}

		Config.routes.callers.api.domain = Config.api[cfg.env] + '/' + Config.routes.navigation.api;
		//Config.routes.callers.api.domain = Config.routes.navigation.host + cfg.sep + cfg.env + host[1] + '.' + host[2] + '/' + Config.routes.navigation.api;
		//Config.routes.callers.api.domain = Config.routes.navigation.host + cfg.sep + cfg.env + host[1] + '.' + host[2] + '/' + Config.routes.navigation.api;
	},
	
	/**
	 * Access service API
	 * @return	{this} 
	 */
	access	: function () { return (this); },
	
	/**
	 * OnPopState callback event handler
	 * @param	{Object}
	 * @return	{void}
	 */
	_onPopState: function (event) {
		this.loop();
	},
	
	/**
	 * LOOOOOOOOOOP on new uri
	 * @return	{void}
	 */
	loop: function () {
		this.pathName = opajs.utils.trim(opajs.utils.trim(this.getHash(), '/'));
		if (!this.pathName) { this.pathName = 'index'; }
		this.spn = this.pathName.split('/');
		this.controller = 'index';
		this.application = 'index';
		this.data = '';
		
		//if (this.pathName === 'index') { this._run(); return; }
		this.controller = this.spn[this.cfg.navigation.controllerIndex];
		this.application = this.spn[this.cfg.navigation.applicationIndex];
		this.data = this.spn[this.cfg.navigation.dataIndex];
		this._run();
	},
	
	/**
	 * History GO, emulate a link
	 * @param	{String} url
	 * @param	{Object} data = null
	 * @param	{String} title = null
	 */
	go: function (url, isPoped, data, title) {
		url = url || this.cfg.navigation.defaultUri; data = data || this.controller; title = title || this.controller;
		window.history.pushState(data, title, url);
		if (isPoped) { this._onPopState(); }
	},
	
	/**
	 * Get the current route token
	 * @return	{String}
	 */
	getHash: function() {
    	return (window.location.pathname);
    },
	
	/**
	 * Run the next loooooooooop
	 * @return	{void}
	 */
	_run: function () {
		var controller = null;
		for (var i = 0; i < this.controllers.length; i++) {
			if (this.controllers[i].id === this.controller) { controller = this.controllers[i]; }
		}

		if(controller && O.srv('auth').isLogged) { controller.instance.__init(true); return; }
		if (!O.srv('auth').isLogged && !Config.application.nologin && this.controller.indexOf("@") < 0) { this.controller = 'login'; }
		else if (!App[this.controller]) { this.controller = 'index'; }
		var ctrl = {id : this.controller, instance : new App[this.controller]()};
		this.controllers.push(ctrl);
		O.ctl(this.controller).__init();
		O.ctl(this.controller).overloaded = true;
	},
	
	/**
	 * Get a controller instance
	 * @param	{String} controllerName
	 * @return	{opajs.helpers.controller}
	 */
	getController: function (controllerName) {
		var controller = {};
		for (var i = 0; i < this.controllers.length; i++) {
			if (this.controllers[i].id === controllerName) { controller = this.controllers[i].instance; }
		}
		return (controller);
	}
};