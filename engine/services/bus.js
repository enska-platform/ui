/**
 * @category	Service
 * @package		src.services
 * @namaspace	opajs
 */

Ext.ns('opajs.services');

opajs.services.bus = {
	
	/**
	 * bus configuration
	 * @type	{Object} 
	 */
	cfg	: {},
	
	/**
	 * Is supported
	 * @type	 {bool} 
	 */
	isSupported	: window.MozWebSocket || window.WebSocket || false,
	
	/**
	 * Openend connections
	 * @type	{int}
	 */
	opened	: 0,
	
	/**
	 * Callbacks
	 * @type	{Array}
	 */
	callbacks: [],
	
	/**
	 * WebSocket instance
	 * @type	{WebSocket} 
	 */
	ws			: null,
	
	/**
	 * URI
	 * @type	{String} 
	 */
	uri			: 'ws://',
	
	/**
	 * TLS enabled
	 * @type	{bool}
	 */
	tls			: false,
	
	/**
	 * connexion status
	 * @type	{bool} 
	 */
	isConnected	: false,
	
	/**
	 * Load the bus zone
	 * @return	{void}
	 */
	load: function () {
		if (Config.bus) { Ext.apply(this.cfg, Config.bus); }
		if (this.isSupported) {
			if (this.cfg.host) {
				if (this.cfg.tls) { this.uri = 'wss://'; }
				this.uri += this.cfg.host + ':' + this.cfg.port;
				if (this.cfg.enabled) { this.connect(); }
			}
		}
		else { this.onError('rt.errors.notSupported'); }
	},
	
	/**
	 * Connect to the host
	 * @return	{void} 
	 */
	connect: function () {
		this.ws = new WebSocket(this.uri);
		this.ws.onopen = this.onConnect;
		this.ws.onclose = this.onClose;
		this.ws.onmessage = this.onReceive;
		this.ws.onerror = this.onError;
	},
	
	/**
	 * Close the current connection
	 * @return	void 
	 */
	disconnect: function (callback) {
		this.send({cmd:'quit'});
	},
	
	
	/**
	 * Helper to send a real-time request
	 * @param	{String}	buffer
	 * @return	void
	 */
	send: function (buffer) {
		this.ws.send(buffer);
	},
	
	/**
	 * Before message receive 
	 * @param	{Object}	evt
	 * @return	void 
	 */
	onReceive: function (e) {
		if ((typeof(e) === 'object') && e.data) {
			var data = {}, msg, id, self = this;
			try {
				data = JSON.parse(e.data);
				id = O.utils.createId(5);
				if (data.meta && data.meta.rted && data.meta.rted.requestId) {
					id = data.meta.rted.requestId;
					msg = O.srv('proxy').proxies.get('message').get(id);
					if (!msg) { msg = new opajs.helpers.proxies.message(); }
				}
				else { msg = new opajs.helpers.proxies.message(); }
				msg.setMessage(e, data);
				O.srv('proxy').proxies.get('message').set(id, msg);
			} catch(e) { warn(e.message); }
		}
		return;
		
	},
	
	/**
	 * Before socket open
	 * @param	{Object}	evt
	 * @return	void 
	 */
	onConnect: function (evt) {
		this.isConnected = true;
	},
	
	/**
	 * Before socket error
	 * @param	{mixed}	evt
	 * @return	void 
	 */
	onError: function (evt) {
		
	},
	
	/**
	 * Before socket close
	 * @param	{Object}	evt
	 * @return	void 
	 */
	onClose: function (evt) {
		
	},
	
	
	/**
	 * Access service API
	 * @return	{this} 
	 */
	access	: function () { return (this); }
};