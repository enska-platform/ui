/**
 * @category	Service
 * @package		src.services
 * @namaspace	opajs
 */

Ext.ns('opajs.services');

opajs.services.auth = {
	
	/**
	 * auth configuration
	 * @type	{Object} 
	 */
	cfg	: {},
	
	/**
	 * App token
	 * @type	{String} 
	 */
	__token__	: null,
	
	/**
	 * App logged-in ?
	 * @type	{Bool} 
	 */
	isLogged	: false,
	
	/**
	 * Load the auth zone
	 * @return	{void}
	 */
	load: function () {
		var self = this;
		if (Config.auth) { Ext.apply(this.cfg, Config.auth); }
		this.__token__ = O.utils.getCookie('__token__');
		if (!this.getToken()) { //&& __CLIENT__ && __CLIENT__.token && __CLIENT__.token === 'switched' || __CLIENT__.token === '') {
			this.setToken(O.utils.createId(2) + '-' + O.utils.createId(2) + '-' + O.utils.createId(2) + '-' + O.utils.createId(1), true);
		}

		O.srv('proxy').request(
			{ uri : '/islogged', ref : 'auth', params : {}, noEncode : true },
			function (meta, records, error, raw, resObj, config) {
				if (error) { console.warn(translate('login.error')); return; }
				self.isLogged = (meta.get('auth') === "SUCCESS") ? true : false;
				if (meta.get('token')) { self.setToken(meta.get('token')); }
				if (self.isLogged) { O.srv('routes').loop(); }
			}, 'isLogged'
		);
	},
	
	/**
	 * Set app token
	 * @param	{String}
	 * @param	{Bool}
	 * @return	{void}
	 */
	setToken	: function (token, remember) {
		remember = remember || false;
		this.__token__ = token || null;
		O.utils.setCookie('__token__', this.getToken(), remember);
	},
	
	/**
	 * Get app token
	 * @return	{String} 
	 */
	getToken	: function () {
		return (this.__token__);
	},

	/**
	 * Delete app token
	 * @return	{void}
	 */
	delToken	: function () {
		this.__token__ = null;
		O.utils.remCookie('__token__');
	},

	/**
	 * Delete app token
	 * @return	{void}
	 */
	refreshToken	: function () {
		this.delToken();
		O.srv('auth').isLogged = false;
		this.load();
	},
	
	/**
	 * Access service API
	 * @return	{this} 
	 */
	access	: function () { return (this); }
};