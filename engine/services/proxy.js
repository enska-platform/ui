/**
 * @category	Service
 * @package		engine.services
 */

Ext.ns('opajs.services');

opajs.services.proxy = {

	/**
	 * Service configuration
	 * @type	{Object} 
	 */
	cfg	: {},
	
	/**
	 * Context' store 
	 */
	store	: {},

	/**
	 * Load the context service
	 * @return	{void}
	 */
	load: function () {
		if (Config.proxy) { Ext.apply(this.cfg, Config.proxy); }
		this.proxies = new opajs.helpers.store({cfg:{id:'opajs_proxies'}});
		this.proxies.set('request', new opajs.helpers.store({cfg:{id:'opajs_xhr_proxy'}}));
		this.proxies.set('message', new opajs.helpers.store({cfg:{id:'opajs_rt_proxy'}}));
	},
	
	/**
	 * Launch RTMessage
	 * @param	{Object}
	 * @param	{Function}
	 * @param	{String}
	 * @return	{void}
	 */
	message: function (config, callback, id) {
		var instance, id = id || O.utils.createId();
		instance = this.proxies.get('message').get(id);
		if (!instance) { instance = new opajs.helpers.proxies.message(); }
		instance.request(config, callback, id);
		this.proxies.get('message').set(id, instance);
		return (id);
	},
	
	/**
	 * Launch XHRRequest
	 * @param	{Object}
	 * @param	{Function}
	 * @param	{String}
	 * @return	{String}
	 */
	request: function (config, callback, id) {
		var instance, id = id || O.utils.createId();
		if (!(instance = this.proxies.get('request').get(id))) {
			instance = new opajs.helpers.proxies.request();
		}
		instance.request(config, callback);
		//this.proxies.get('request').del(id);
		this.proxies.get('request').set(id, instance);
		return (id);
	},
	
	/**
	 * Authentification check
	 * @param	{Object}
	 * @param	{Function}
	 * @return	{void} 
	 */
	auth: function () { return; },
	
	/**
	 * Access to the store
	 * @return	opajs.helpers.store 
	 */
	access	: function () {
		return (this);
	}
};