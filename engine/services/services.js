/**
 * @category	Service
 * @package		src.services
 * @namaspace	opajs
 */

Ext.ns('opajs.services');

opajs.services.services = {
	
	/**
	 * Service configuration
	 * @type	{Object} 
	 */
	cfg	: {},
	
	/**
	 * Services instances
	 * @type	{Array} 
	 */
	services	: [],
	
	/**
	 * Load the service engine
	 * @return	{void}
	 */
	load: function () {
		this.cfg = {
			core	: [],
			app		: []
		};
		
		if (Config.services) { Ext.apply(this.cfg, Config.services); }
		this._loadGlobals();
	},
	
	loadService	: function (service) {
		
	},
	
	/**
	 * Access service API
	 * @return	{this} 
	 */
	access	: function () { return (this); },
	
	/**
	 * Get a service instance
	 * @param	{String} serviceName
	 * @return	{App.services}
	 */
	get: function (serviceName) {
		if (Ext.isArray(this.services)) {
			for (var i = 0; i < this.services.length; i++) {
				if (this.services[i].id === serviceName) {
					return(this.services[i].instance);
				}
			}
		}
	},
	
	/**
	 * Include a script
	 * @param	{string}
	 * @param	{function}
	 * @return	{void}
	 */
	include : function (serviceName, callback, serviceType) {
		serviceType = serviceType || 'core';
		var url = Config.routes.callers.def;
		url = (serviceType === 'core') ? url.protocol + '://' + url.domain + '/opajs/custom/services/' : url.protocol + '://' + url.domain + '/webapps/' + Config.application.name + '/services/';
		url = url + serviceName + '.js';
		O.utils.include(url, callback, serviceName);
	},
	
	/**
	 * Load the globals services
	 * @return	{void}
	 */
	_loadGlobals: function () {
		if (Ext.isArray(this.cfg.core)) { this._loadCoreServices(); }
		if (Ext.isArray(this.cfg.app)) { this._loadAppServices(); }
	},
	
	/**
	 * Load the globals core services
	 * @return	{void}
	 */
	_loadCoreServices: function () {
		for (var i=0; this.cfg.core.length > i; i++) {
			try {
				var self = this;
				this.include(this.cfg.core[i], function (service) {
					var obj, srv = new opajs.services[service]();
					obj = {id: service, instance: srv};
					srv.setId(service);
					if (srv.load) { srv.load(); }
					self.services.push(obj);
				});
			} catch(e){console.log(e.stack);console.warn('OPAJS::Unable to load the CORE service: ' + this.cfg.core[i] + "\n");}
		}
	},
	
	/**
	 * Load the globals core services
	 * @return	{void}
	 */
	_loadAppServices: function () {
		for (var i=0; this.cfg.app.length > i; i++) {
			try {
				var self = this;
				this.include(this.cfg.app[i], function (service){
					var obj, srv = new App.services[service]();
					obj = {id: service, instance: srv};
					srv.setId(service);
					if (srv.load) { srv.load(); }
					self.services.push(obj);
				}, 'app');
			} catch(e){console.log(e.stack);console.warn('OPAJS::Unable to load the APP service: ' + this.cfg.app[i] + "\n");}
		}
	}
};