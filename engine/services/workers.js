/**
 * @category	Service
 * @package		src.services
 * @namaspace	opajs
 */

Ext.ns('opajs.services');

opajs.services.workers = {
	
	/**
	 * bus configuration
	 * @type	{Object} 
	 */
	cfg	: {},
	
	/**
	 * Is supported
	 * @type	 {bool} 
	 */
	isSupported	: window.MozWorker || window.Worker || false,
	
	/**
	 * Workers instances
	 * @type	{array}
	 */
	workers			: [],
	
	/**
	 * Load the workers zone
	 * @TODO : Statics workers
	 * @return	{void}
	 */
	load: function () {
		if (Config.workers) { Ext.apply(this.cfg, Config.workers); }
		if (!this.isSupported) { this.onError('workers.errors.notSupported'); return; }
	},
	
	/**
	 * Start a worker (and create if not exists)
	 * @param	{String}
	 * @param	{function}
	 * @return	{void}
	 */
	start	: function (worker, config, _onmessage, id) {
		var id = id || O.utils.createId(2), src = (config && config.src) ? this.cfg.paths[config.src] : this.cfg.paths.core,
		src = src + worker + '.js', config = (config) ? config : {};
		
		if (!this.workers[id]) { this.workers[id] = new Worker(src); }
		this.workers[id].onmessage = function (event) { _onmessage(event) };
		this.workers[id].onerror = function (event) { console.log(event); };
		config.id = id; this.workers[id].postMessage(config);
	},
	
	/**
	 * Stop a worker
	 * @param	{String}
	 * @param	{bool}
	 * @return	{void}
	 */
	stop	: function (id) {
		if (!this.workers[id]) { return; }
		this.workers[id].terminate();
		this.workers[id] = null;
		delete(this.workers[id]);
	},
	
	/**
	 * Access service API
	 * @return	{this} 
	 */
	access	: function () { return (this); }
};