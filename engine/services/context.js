/**
 * @category	Service
 * @package		engine.services
 */

Ext.ns('opajs.services');

opajs.services.context = {

	/**
	 * Service configuration
	 * @type	{Object} 
	 */
	cfg	: {},
	
	/**
	 * Context' store 
	 */
	store	: {},

	/**
	 * Load the context service
	 * @return	{void}
	 */
	load: function () {
		if (Config.context) { Ext.apply(this.cfg, Config.context); }
		this.store = new opajs.helpers.store({cfg:{id:'opajs_context'}});
		this.store.load();
	},
	
	/**
	 * Access to the store
	 * @return	opajs.helpers.store 
	 */
	access	: function () {
		return (this.store);
	}
};