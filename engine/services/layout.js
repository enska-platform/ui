/**
 * @category	Service
 * @package		engine.services
 * @namaspace	opajs.services
 */

Ext.ns('opajs.services');

opajs.services.layout = {
	
	/**
	 * Service configuration
	 * @type	{Object} 
	 */
	cfg			: {},
	
	/**
	 * Default viewport
	 * @type	{Object} 
	 */
	viewport	: {},
	
	/**
	 * Application views
	 * @type	{opajs.helpers.store}
	 */
	views		: {},
	
	/**
	 * Application widgets
	 * @type	{opajs.helpers.store}
	 */
	widgets		: {},
	
	/**
	 * replace array
	 * @type	{array}
	 */
	replace		: [],

	/**
	 * Load the layout service
	 * @return	{void}
	 */
	load: function () {
		this.cfg.engine = Config.application.bootstrap;
		this.views = new opajs.helpers.store();
		this.widgets = new opajs.helpers.store();
		if (Config.layout) { Ext.apply(this.cfg, Config.layout); }
	},
	
	/**
	 * Access service API
	 * @return	{this} 
	 */
	access	: function () { return (this); },
	
	/**
	 * Init the main viewport
	 * @return	{void} 
	 */
	initViewport: function () {
		if (opajs.helpers.viewports[this.cfg.engine]) {
			this.viewport = new opajs.helpers.viewports[this.cfg.engine](this.cfg);
		}
	},
	
	/**
	 * Render the main viewport
	 * @return	{void} 
	 */
	render: function () {
		this.viewport.render();
	},
	
	/**
	 * Add a view to catalog
	 * @param	{String}
	 * @param	{Object}
	 * @return	{void} 
	 */
	add	: function (id, view) {
		this.views.set(id, view);
		this.viewport.addView(id);
	},
	
	/**
	 * Activate a specific view
	 * @param	{String}
	 * @return	{void} 
	 */
	activate	: function (view) {
		this.viewport.activate(view);
	},
	
	/**
	 * Desctivate a specific view
	 * @param	{String}
	 * @return	{void} 
	 */
	desactivate	: function (view) {
		this.viewport.desactivate(view);
	},
	
	/**
	 * Refresh viewport
	 * @param	{String}
	 * @param	{Bool} = true
	 * @return	{void} 
	 */
	display	: function (config) {
		this.viewport.display(config);
	},
	
	/**
	 * Get a view instance
	 * @param	{String}
	 * @return	{mixed}
	 */
	getView: function (viewName) {
		return (this.views.get(viewName));
	},
	
	/**
	 * Get a widget instance
	 * @param	{String}
	 * @return	{mixed}
	 */
	getWidget: function (widgetName) {
		return (this.widgets.get(widgetName));
	},
	
	/**
	 * Get a template
	 * @param	{String}
	 * @return	{mixed}
	 */
	getTpl: function (tplName) {
		return (this.viewport.getTpl(tplName));
	},
	
	/**
	 * Translate template
	 * @param	{String} raw template
	 * @param	{String} selected language
	 * @return	{String}
	 */
	translate	: function (html, local) {
		var r = (local) ? 'I18N_' + local : 'I18N_' + Config.i18n.def;
		try { r = eval(r); }
		catch(e) { r = [] }
		return (this.templatize(html, r, '{T}', '{/T}'));
	},
	
	/**
	 * Prepare the templatization
	 * @param	{String}	buffer
	 * @param	{mixed}		data
	 * @param	{String}	_delimiterStart = '{{ '
	 * @param	{String}	_delimiterEnd = ' }}'
	 * @return	{array}
	 */
	templatize	: function (buffer, data, _delimiterStart, _delimiterEnd) {
		this.replace = [];
		this.__getFormat(data, _delimiterStart, _delimiterEnd);
		return (this.__templatize(buffer, this.replace, _delimiterStart, _delimiterEnd));
	},

	/**
	 * Do the templatization
	 * @param	{String}	buffer
	 * @param	{Array}		replace
	 * @param	{String}	_delimiterStart
	 * @param	{String}	_delimiterEnd
	 * @return	{String}
	 */
	__templatize	: function (buffer, replace, _delimiterStart, _delimiterEnd) {
		var res = index = '';
		var len = O.utils.strlen(buffer);
		var lenStart = O.utils.strlen(_delimiterStart);
		var lenEnd = O.utils.strlen(_delimiterEnd);
		var i = j = end = 0;

		while (i < len) {
			if (buffer.substr(i, lenStart) === _delimiterStart) {
				index = '', j = end = i + lenStart;
				while (j < len) {
					if (buffer.substr(j, lenEnd) !== _delimiterEnd) { index += buffer[j]; }
					else { i = j + lenEnd; index = _delimiterStart + index + _delimiterEnd; break; }
					j++;
				}
				var t = this.__getReplace(index, replace);
				res += t;
			}
			res += buffer[i++];
		}
		return (res);
	},
	
	/**
	 * Get format for local templatization
	 * @param	{Object}	data
	 * @param	{String}	_delimiterStart = '{{ '
	 * @param	{String}	_delimiterEnd = ' }}'
	 * @param	{Array}		_position = [];
	 * @return	{array}
	 */
	__getFormat: function (data, _delimiterStart, _delimiterEnd, _position) {
		_delimiterStart = _delimiterStart || '{{ ';
		_delimiterEnd = _delimiterEnd || ' }}';
		if ((typeof(data) === 'object') || (typeof(data) === 'array')) {
			if (!Ext.isArray(_position)) { _position = []; }
			for (var i in data) {
				_position.push(i);
				if ((typeof(data[i]) === 'object') || (typeof(data[i]) === 'array')) {
					this.__getFormat(data[i], _delimiterStart, _delimiterEnd, _position);
				}
				else if (typeof(data[i]) === 'string') {
					this.replace.push({
						search	: _delimiterStart + _position.join('.') + _delimiterEnd,
						text	: data[i].htmlDecode()
					});
				}
				_position.pop();
			}
		}
	},
	
	/**
	 * Get replace value from key
	 * @param	{String}	key
	 * @param	{Array}		replace
	 * @return	{String}
	 */
	__getReplace : function(key, replace) {
		for (var i = 0; i < replace.length; i++) { if (replace[i].search === key) { return (replace[i].text); }}
		return (key);
	}
	
}