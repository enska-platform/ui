/**
 * @category	Service
 * @package		src.services.workers
 * @namaspace	opajs
 */

onmessage = function(event){
	xhr = new XMLHttpRequest();
	
	xhr.upload.addEventListener('progress', function(e) {
		var value = parseInt(100 - (e.loaded / e.total * 100));
		postMessage({data:value, callback:'onprogress'});
	}, false);
	 
	xhr.upload.addEventListener('loadend', function(e) {
		postMessage({data:event.data.id, callback:'oncomplete'});
	}, false);

	xhr.open('POST', event.data.remote, true); xhr.setRequestHeader('X-FILENAME', event.data.file.name); 
	xhr.send(event.data.file);
};
