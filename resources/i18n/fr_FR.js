/**
 *Copyright (C) 2008 - 2012  EnsKaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	li18n
 * @package		app.li18n
 * @copyright	Copyright (c) 2008 - 2012 EnsKaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

LI18N_fr_FR = [];

LI18N_fr_FR['index'] = {
	
};

LI18N_fr_FR['instance'] = {
	errors	: {
		deploy	: {
			dnsA	: 'Le nom de domaine n\'existe pas'
		}
	}
};

LI18N_fr_FR['account'] = {
	
};



LI18N_fr_FR['commons'] = {
	fields	: {
		errors	: {
			error	: 'Error lors de la saisie du formulaire',
			email	: 'L\'email saisie est invalide'
		}
	},
	users	: {
		errors:	{
			unknownUser	: 'Utilisateur inconnu',
			exists		: 'L\'utilisateur est déjà enregistré',
			acl			: 'Vous n\'avez pas le niveau de droit suffisant'
		}
	},
	db		: {
		errors	:{
			insert	: 'La connexion à la base de données à été intrompue'
		}
	},
	login	: {
		protocol	: {
			"UNAUTHORIZED"	: 'L\'authentification à échouée'
		}
	},
	api		: {
		errors	: {
			closed	: 'L\'API d\'authentification est fermée'
		}
	}
};